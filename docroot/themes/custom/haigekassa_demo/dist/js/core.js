(function ($) {
    $(document).ready(function (e) {
        // Category select
        $('.age-tabs').on('click', '.age-tab', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var btn = $(this),
                submenu = $(this).has('.sub-menu').length > 0,
                content = btn.attr('href');
            $('.age-tabs').find('.u-active').removeClass('u-active');
            if (submenu) {
                btn.find('.age-tab').first().trigger('click');
            } else {
                var p = btn.parent();
                btn.addClass('u-active');
                if (p.is('.sub-menu')) {
                    p.parent().addClass('u-active');
                }
                $('.age-tabs-content .age-tabs-content-set.active').removeClass('active');
                $(content).addClass('active');
            }
            // Hide all item details blocks
            $(".paragraph--type--p-insurance-block .item-details").hide();
        });
        // Mobile category toggles (right)
        $('.age-tabs .toggle.r').on('click', function (e) {
            var current = $('.age-tabs').children('.u-active'),
                next;

            next = current.next('.age-tab');
            if (!next.length) {
                next = $('.age-tabs').children('.age-tab').first();
            }

            next.click();
        });
        // Mobile category toggles (left)
        $('.age-tabs .toggle.l').on('click', function (e) {
            var current = $('.age-tabs').children('.u-active'),
                next;

            next = current.prev('.age-tab');
            if (!next.length) {
                next = $('.age-tabs').children('.age-tab').last();
            }
            next.click();
        });
        // More details pop-up
        $('.more-details').click(function (e) {
            e.preventDefault();
            var link = $(this),
                row = link.closest('.item-row'),
                info = row.find('.item-details'),
                html = info.html();
            info.slideToggle(300);
        });
        // Reference instance group button
        $('.age-tabs-content-set a.insurance-group-reference').click(function (e) {
            e.preventDefault();
            // cutting nid from string like #insurance-group-xxx
            var insuranceGroupNid = $(this).attr('href').substr(17);

            // making our way up to age_tab
            var insuranceGroup = $('.paragraph--type--p-insurance-block').find("[data-insurance-group='" + insuranceGroupNid + "']");
            var age_tab_content = insuranceGroup.closest('.age-tabs-content-set');
            var age_tab = $('.age-tabs').find("[href='#" + age_tab_content.attr('id') + "']");

            // finally clicking it
            age_tab.trigger('click');
        });

        //initialize first click to show some content
        $('.age-tabs').find('.age-tab').first().trigger('click');
    });
})(jQuery);


//# sourceMappingURL=core.js.map